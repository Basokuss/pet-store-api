# frozen_string_literal: true

class PetsController < ApplicationController
  before_action :set_pet, only: [:show]

  # GET /pets
  def index
    limit = pets_params[:limit].to_i # .nil? ? max  : (pets_params[:limit].to_i > max ? max : pets_params[:limit].to_i)

    if limit.negative? || limit > 100
      render json: { message: 'Invalid limit', code: 422 }, status: :unprocessable_entity
      return
    end

    @pets = Pet.order(:id).limit(limit)

    # Test Header
    response.headers['X-Next'] = "/pets?limit=#{limit}&page=2"

    render json: @pets
  end

  # GET /pets/1
  def show
    render json: @pet
  end

  # POST /pets
  def create
    Rails.logger.debug 'Params'
    Rails.logger.debug params
    @pet = Pet.new(pet_params)

    if @pet.save
      render json: @pet, status: :created, location: @pet
    else
      render json: { message: @pet.errors, code: 422 }, status: :unprocessable_entity
      # render json: @pet.errors, status: :unprocessable_entity
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_pet
    @pet = Pet.find(params[:id])
  rescue ActiveRecord::RecordNotFound => e
    # render json: e.message, status: :not_found
    render json: { message: e.message, code: 404 }, status: :not_found
  end

  # Only allow a trusted parameter "white list" through.
  def pet_params
    params.require(:pet).permit(:name, :tag)
  end

  def pets_params
    params.permit(:page, :limit)
  end
end
