# Pet store API
API Test project

## Installation
1. install  [Ruby](https://www.ruby-lang.org/es/documentation/installation/) 2.6.9
2. Install bundler gem
	```bash
	gem install bundler
	```
3. install Rails 2.3.8
	```bash
	gem install rails -v 2.3.8
	```
4. Install some database, like [PostgreSQL](https://www.postgresql.org/download/) or [MySQL](https://dev.mysql.com/doc/mysql-getting-started/en/)

## Usage
In a terminal go to the Project's root folder.
### Execute bundler(if necessary):
```bash
bundle install
```
###  Create database and run migrations
```bash
rails db:create
rails db:migrate
rails db:seed
```
Currently the project is configured to work with PostgreSQL, but you can configure the app to use another DB.

### Test with RSpec
```bash
rspec
```
### Generate the Swagger JSON files
```bash
SWAGGER_DRY_RUN=0 RAILS_ENV=test rails rswag
```
### Execute Rubocop
```bash
rubocop -A
```

### Open Coverage Tool
Go to Project's Root folder and execute the command
Debian/Ubuntu:
```bash
xdg-open coverage/index.html
```

### Run server
```bash
rails s
```

### Open Swagger Docs
While the server is running:
[API Doc](http://localhost:3000/api-docs/index.html)

### Project's Git

[Pet store API ](https://gitlab.com/Basokuss/pet-store-api/-/tree/master)

## License
[Unlicense](https://choosealicense.com/licenses/unlicense/)
