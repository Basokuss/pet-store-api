# frozen_string_literal: true

require 'swagger_helper'

RSpec.describe 'pets', type: :request do
  path '/pets' do
    get('list pets') do
      tags 'pets'
      operationId 'listPets'
      consumes 'application/json'
      parameter name: :limit, in: :query, description: 'How many items to return at one time (max 100)',
                required: false, schema: { type: :integer, format: :int32 }

      response(200, 'A paged array of pets') do
        header 'X-Next', description: 'A link to the next page of responses', schema: { type: :string }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              schema: { '$ref' => '#/components/schemas/Pets' }
            }
          }
        end

        run_test!
      end

      response(422, 'unexpected error') do
        let(:limit) { -11 }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              schema: { '$ref' => '#/components/schemas/Error' }
            }
          }
        end

        run_test!
      end
    end

    post('create pet') do
      tags 'pets'
      operationId 'createPets'
      consumes 'application/json'
      parameter name: :pet, in: :body

      response(201, 'Null response') do
        let(:pet) { { name: 'Pet', tag: 'Test' } }

        run_test!
      end

      response(422, 'unexpected error') do
        let(:pet) { { tag: 'bad pet' } }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              schema: { '$ref' => '#/components/schemas/Error' }
            }
          }
        end

        run_test!
      end
    end
  end

  path '/pets/{id}' do
    get('show pet') do
      tags 'pets'
      operationId 'showPetById'
      parameter name: 'id', in: :path, required: true, description: 'The id of the pet to retrieve', type: :string

      response(200, 'Expected response to a valid request') do
        let(:id) { '1' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              schema: { '$ref' => '#/components/schemas/Pet' }
            }
          }
        end

        run_test!
      end

      response(404, 'unexpected error') do
        let(:id) { 1000 }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              schema: { '$ref' => '#/components/schemas/Error' }
            }
          }
        end

        run_test!
      end
    end
  end
end
