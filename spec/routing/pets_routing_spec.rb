# frozen_string_literal: true

require 'rails_helper'

RSpec.describe PetsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/pets').to route_to('pets#index')
    end

    it 'routes to #show' do
      expect(get: '/pets/1').to route_to('pets#show', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/pets').to route_to('pets#create')
    end
  end
end
